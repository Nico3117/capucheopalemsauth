import express from 'express';
const app = express();
const port = 3000; // default port to listen
const dotenv = require('dotenv')
dotenv.config()

const cors = require('cors')
app.use(cors())

// middleware used to parse incoming requests with JSON payloads
app.use(express.json())

import userController from "./controllers/user.controller";
import authenticationController from "./controllers/auth.controller";

app.use('/api/login', authenticationController.loginRouter)
app.use('/api/sign-up', authenticationController.signupRouter)
app.use('/api/user', userController)

const env = process.env.NODE_ENV || 'development'
const httpScheme = env !== 'development' ? 'https' : 'http'
const host = env !== 'development' ? 'redpegasus-micro-auth.herokuapp.com' : 'localhost'

// swagger configuration
const swaggerJSDoc = require('swagger-jsdoc');

let swaggerUrl = `${httpScheme}://${host}`
swaggerUrl += env === 'development' ?  ':' + port : ''

const swaggerDefinition: any = {
    openapi: '3.0.0',
    info: {
        title: 'Capucheopale authentication',
        version: '1.0.0',
        description: 'This is REST API for Capucheopale authentication'
    },
    components: {
        securitySchemes: {
            jwt: {
                type: 'http',
                name: 'Authorization',
                scheme: 'bearer',
                in: 'header',
                bearerFormat: 'JWT',
            }
        }
    },

    servers: [
        {
            url: swaggerUrl
        }
    ]
};

const options = {
    swaggerDefinition,
    // Paths to files containing OpenAPI definitions
    apis: ['**/controllers/*.js', './controllers/*.ts'],
};

const swaggerSpec = swaggerJSDoc(options);
const swaggerUi = require('swagger-ui-express');

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.get('/swagger.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.send(swaggerSpec)
})

// start the Express server
app.listen(process.env.PORT || port);
