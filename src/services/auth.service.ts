import {UserDao} from "../dao/user.dao";
import {UsersService} from "./users.service";
import {UnauthorizedError} from "../errors/unauthorized.error";
import {ConflictError} from "../errors/conflict.error";
import {NotFoundError} from "../errors/not-found.error";

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;

export class AuthService {
    private userDAO: UserDao = new UserDao()

    public async login (login: string, password: string) {
        if (!login || !password) {
            throw new Error('all inputs are required');
        }

        const user = await this.userDAO.getByLogin(login);
        if(user) {
            const match = await bcrypt.compare(password, user.mot_de_passe);
            if(match) {
                const token = jwt.sign(
                    { user },
                    process.env.TOKEN_KEY,
                    {
                        expiresIn: "2h"
                    }
                )

                return {
                    token
                }
            }
            throw new UnauthorizedError()
        }
        throw new NotFoundError()
    }

    public async signup (user) {
        if (!UsersService.checkUserToCreateIsValid(user)) {
            throw new Error('invalid user');
        }

        const userAlreadyExists = await this.userDAO.getByLogin(user.login);
        if(userAlreadyExists) {
            throw new ConflictError()
        }

        bcrypt.hash(user.mot_de_passe, saltRounds).then((hash) => {
            user.mot_de_passe = hash
            this.userDAO.create(user)
        });

        return user;
    }
}
