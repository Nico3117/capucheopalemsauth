import { UserDao } from '../dao/user.dao';
import {NotFoundError} from "../errors/not-found.error";
import {ConflictError} from "../errors/conflict.error";

const uuid = require('uuid')
const bcrypt = require('bcrypt');
const saltRounds = 10;

export class UsersService {
    private userDAO: UserDao = new UserDao()

    public async getAllUsers() {
        return await this.userDAO.list()
    }

    public async getUser(userId: string) {
        const user = await this.userDAO.getByID(userId);
        if (!user) {
            throw new NotFoundError('unknown user')
        }
        return user
    }

    public async createUser(user) {
        if (!UsersService.checkUserToCreateIsValid(user)) {
            throw new Error('invalid user');
        }

        const userAlreadyExists = await this.userDAO.getByLogin(user.login);
        if(userAlreadyExists) {
            throw new ConflictError()
        }

        return await this.userDAO.create(user);
    }

    public async deleteUser(userID: string) {
        const user = await this.userDAO.getByID(userID);
        if (!user) {
            throw new NotFoundError('unknown user')
        }
        return await this.userDAO.delete(userID);
    }

    public async updateUser(id, user) {
        const existingUser = await this.userDAO.getByID(id);
        if (!existingUser) {
            throw new NotFoundError()
        }
        if(user.role_id) {
            throw new Error('cannot change role_id')
        }
        if(user.login) {
            throw new Error('cannot change login')
        }

        const hash = await bcrypt.hash(user.mot_de_passe, saltRounds)
        user.mot_de_passe = hash

        return await this.userDAO.update(id, user)
    }

    public static checkUserToCreateIsValid(user) {
        return user && user.login && user.mot_de_passe && user.prenom && user.nom
    }

}
