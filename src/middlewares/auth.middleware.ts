const jwt = require('jsonwebtoken');

const config = process.env;

export default {
    verifyToken: (req, res, next) => {
        const token = req.headers.authorization ?
            req.headers.authorization.replace('Bearer ', '') : null;

        if (!token) {
            return res.status(403).send("Bearer token requis");
        }
        try {
            req.user = jwt.verify(token, config.TOKEN_KEY).user
        } catch (err) {
            return res.status(401).send("Token invalide");
        }
        return next();
    },

    isAdmin : (req, res, next) => {
        const token = req.headers.authorization ?
            req.headers.authorization.replace('Bearer ', '') : null;

        if (!token) {
            return res.status(403).send("Bearer token requis");
        }

        const decoded = jwt.verify(token, config.TOKEN_KEY).user;
        const userRole = decoded.role_id;

        if(userRole !== '3') {
            return res.status(403).send("Accès refusé");
        }
        return next();
    }
}
