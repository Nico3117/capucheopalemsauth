const { Model, DataTypes } = require("sequelize");
import databaseConnection from "../services/database-connection";

const User = databaseConnection.define("user", {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
    },
    login: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    nom: {
        type : DataTypes.TEXT,
        allowNull: false
    },
    prenom: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    argent: DataTypes.INTEGER,
    mot_de_passe: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    role_id: {
        type: DataTypes.INTEGER
    }
    }, {
        tableName: 'personne',
        timestamps: false
    });

(async () => {
    try {
        await databaseConnection.sync({ force: false });
    } catch (err) {
        console.log(err)
    }
})();

export default User
