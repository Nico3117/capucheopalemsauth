import User from '../models/user.model'

export class UserDao {
    constructor() {
    }

    public async list() {
        return await User.findAll();
    }

    public async create(user) {
        return await User.create(user);
    }

    public async delete(userID: string) {
        const user = await this.getByID(userID);
        if (user) {
            await user.destroy()
            return userID;
        }
    }

    public async update(id, modifiedUser) {
        const user = await this.getByID(id);
        if (user) {
            await user.update(modifiedUser)
            await user.save()
            return user
        }
    }

    public async getByLogin(login: string) {
        return await User.findOne({
            where: {
                login
            }
        });
    }

    public async getByID(userID: string) {
        return await User.findOne({
            where: {
                id: userID
            }
        });
    }
}
