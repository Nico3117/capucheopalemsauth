import { Router } from 'express';
import { AuthService } from "../services/auth.service";
import {UnauthorizedError} from "../errors/unauthorized.error";
import {ConflictError} from "../errors/conflict.error";
import {NotFoundError} from "../errors/not-found.error";

const loginRouter = Router();
const signupRouter = Router();
const authService = new AuthService()

/**
 * @openapi
 * /api/login:
 *   post:
 *     summary: Login
 *     tags:
 *        - Authentication
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               login:
 *                 type: string
 *               mot_de_passe:
 *                 type: string
 *             required:
 *               - login
 *               - mot_de_passe
 *     responses:
 *      '200':
 *        description: success
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                token:
 *                  type: string
 *      '404':
 *        description: Login inconnu
 *      '401':
 *        description: Mot de passe incorrect
 *      '400':
 *        description: Erreur dans la requête
 */
loginRouter.post('/', async (request, response) => {
    try {
        const user = await authService.login(request.body.login, request.body.mot_de_passe)
        response.send(user)
    } catch (error) {
        if (error instanceof NotFoundError) {
            response.status(404).send('Login inconnu')
        } else if (error instanceof UnauthorizedError) {
            response.status(401).send('Mot de passe incorrect')
        } else {
            response.status(400).send(error.message)
        }
    }
})

/**
 * @openapi
 * /api/sign-up:
 *   post:
 *     summary: Signup
 *     tags:
 *        - Authentication
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               login:
 *                 type: string
 *               mot_de_passe:
 *                 type: string
 *               nom:
 *                 type: string
 *               prenom:
 *                 type: string
 *             required:
 *               - login
 *               - mot_de_passe
 *     responses:
 *      '200':
 *        description: Success
 *      '409':
 *        description: Login déjà existant
 *      '400':
 *        description: Erreur dans la requête
 */
signupRouter.post('/', async (req, res) => {
    try {
        const user = await authService.signup({ role_id: '2', ...req.body })
        res.status(200).send(user)
    } catch (error) {
        if(error instanceof ConflictError) {
            res.status(409).send('Login déjà existant')
        } else {
            res.status(400).send(error.message)
        }
    }
})

export default {
    loginRouter,
    signupRouter,
};
