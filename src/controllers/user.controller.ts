import { Router } from 'express';
import { UsersService } from '../services/users.service';
const userController = Router();
import auth from '../middlewares/auth.middleware';
import {NotFoundError} from "../errors/not-found.error";
import {ConflictError} from "../errors/conflict.error";

const usersService = new UsersService();

userController.get('/', [auth.verifyToken, auth.isAdmin], async (req, res) => {
    const users = await usersService.getAllUsers();
    res.status(200).send(users);
})

userController.post('/',[auth.verifyToken, auth.isAdmin], async (req, res) => {
    try {
        const user = await usersService.createUser(req.body)
        res.status(200).send(user)
    } catch (error) {
        if(error instanceof ConflictError) {
            res.status(409).send('Login already exists')
        } else {
            res.status(400).send(error.message)
        }
    }
})

/**
 * @openapi
 * /api/user/{userId}:
 *   get:
 *     security:
 *       - jwt: []
 *     summary: Get user
 *     tags:
 *        - User
 *     parameters:
 *       - in: path
 *         name: userId
 *         required: true
 *         schema:
 *           type: integer
 *           minimum: 1
 *         description: The user ID
 *     responses:
 *       '200':
 *         description: Success
 *       '404':
 *         description: Utilisateur inconnu
 *       '403':
 *         description: Bearer token requis ou Impossible de voir le compte d'un autre utilisateur
 *       '401':
 *         description: Token invalide
 *       '400':
 *         description: Erreur dans la requête
 */
userController.get('/:userID', [auth.verifyToken], async (req, res) => {
   try {
       if (req.params.userID !== req.user.id.toString()) {
           res.status(403).send('Impossible de voir le compte d\'un autre utilisateur');
       }
       const user = await usersService.getUser(req.params.userID);
       res.status(200).send(user);
   } catch (error) {
       if (error instanceof NotFoundError) {
           res.status(404).send('Utilisateur inconnu')
       } else {
           res.status(400).send(error.message)
       }
   }
})

/**
 * @openapi
 * /api/user/{userId}:
 *   put:
 *     security:
 *       - jwt: []
 *     summary: Edit user
 *     tags:
 *        - User
 *     parameters:
 *      - in: path
 *        name: userId
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 1
 *        description: The user ID
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               mot_de_passe:
 *                 type: string
 *               nom:
 *                 type: string
 *               prenom:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Success
 *       '404':
 *         description: Utilisateur inconnu
 *       '403':
 *         description: Bearer token requis ou Impossible d'éditer un autre utilisateur
 *       '401':
 *         description: Token invalide
 *       '400':
 *         description: Erreur dans la requête
 */
userController.put('/:userID', [auth.verifyToken], async (req, res) => {
    try {
        if (req.params.userID !== req.user.id.toString()) {
            res.status(403).send('Impossible d\'éditer un autre utilisateur');
        }
        const user = await usersService.updateUser(req.params.userID, req.body);
        res.status(200).send(user);

    } catch (error) {
        if (error instanceof NotFoundError) {
            res.status(404).send('Utilisateur inconnu')
        } else {
            res.status(400).send(error.message)
        }
    }
})


userController.delete('/:userID', [auth.verifyToken, auth.isAdmin], async (req: any, res) => {
    try {
        await usersService.deleteUser(req.params.userID)
    } catch (error) {
        if (error instanceof NotFoundError) {
            res.status(404).send('Utilisateur inconnu')
        } else {
            res.status(400).send(error.message)
        }
    }
})

export default userController;
